public class NodeList {

    private Node head;

    public NodeList() {
        this.head = null;
    }

    public void insertNodeAtIndex(int index, String value) {
        Node left = null;
        Node right = null;
        if (index == 0) {
            right = head;
        } else {
            left = getNodeAtIndex(index - 1);
            right = left.getNext();
        }

        Node inserted = new Node(value);
        if (left != null) {
            left.setNext(inserted);
            inserted.setPrevious(left);
        }
        if (right != null) {
            inserted.setNext(right);
            right.setPrevious(inserted);
        }
        if (index == 0) {
            head = inserted;
        }
    }

    public void deleteNodeAtIndex(int index) {
        Node nodeToRemove = getNodeAtIndex(index);
        Node left = nodeToRemove.getPrevious();
        Node right = nodeToRemove.getNext();

        if (left != null) {
            left.setNext(right);
        } else {
            head = right;
        }
        if (right != null) {
            right.setPrevious(left);
        }
    }

    public void replaceNodeAtIndex(int index, String value) {
//        deleteNodeAtIndex(index);
//        insertNodeAtIndex(index, value);
        Node nodeToReplace = getNodeAtIndex(index);
        Node left = nodeToReplace.getPrevious();
        Node right = nodeToReplace.getNext();

        Node newNode = new Node(value);
        if (left != null) {
            left.setNext(newNode);
            newNode.setPrevious(left);
        }
        if (right != null) {
            newNode.setNext(right);
            right.setPrevious(newNode);
        }
        if (index == 0) {
            head = newNode;
        }
    }

    private Node getNodeAtIndex(int index) {
        Node current = head;
        for (int currentIndex = 0; currentIndex != index && current != null; currentIndex++) {
            current = current.getNext();
        }
        if (current == null) {
            throw new RuntimeException("Index out of bounds");
        }
        return current;
    }

    @Override
    public String toString() {
        Node node = head;
        StringBuilder result = new StringBuilder();
        while (node != null) {
            result.append(node.getValue());
            node = node.getNext();
        }

        return result.toString();
    }

}

//        Node left = null;
//        Node right = head;
//        int currentIndex = 0;
//        while (currentIndex != index) {
//            if (right == null) {
//                throw new RuntimeException("Index out of bounds");
//            }
//            currentIndex++;
//            left = right;
//            right = right.getNext();
//        }