public class Main {
    public static void main(String[] args) {

        List list = new List();

        for(int i = 0; i < 5; i++){
            list.addElementAtIndex(i,i + 19);
        }

        list.addElementAtIndex(4 ,21241);
        list.deleteElementAtIndex(4);
        list.printArray();

        NodeList nodeList = new NodeList();
        nodeList.insertNodeAtIndex(0, "2");
        nodeList.insertNodeAtIndex(1, "4");
        nodeList.insertNodeAtIndex(1, "3");
        nodeList.insertNodeAtIndex(0, "1");
        assertEquals(nodeList.toString(), "1234");
        nodeList.replaceNodeAtIndex(0, "5");
        nodeList.replaceNodeAtIndex(3, "6");
        nodeList.replaceNodeAtIndex(1, "7");
        assertEquals(nodeList.toString(), "5736");
        nodeList.deleteNodeAtIndex(3);
        nodeList.deleteNodeAtIndex(1);
        assertEquals(nodeList.toString(), "53");
        nodeList.deleteNodeAtIndex(0);
        assertEquals(nodeList.toString(), "3");
        nodeList.deleteNodeAtIndex(0);
        assertEquals(nodeList.toString(), "");

        boolean hasFailed = false;
        try {
            nodeList.deleteNodeAtIndex(0);
        } catch (Exception e) {
            hasFailed = true;
        }
        assertTrue(hasFailed);

    }

    public static void assertEquals(String value1, String value2) {
        if (!value1.equals(value2)) {
            throw new RuntimeException("Value 1 = " + value1 + " Value 2 = " + value2);
        }
    }

    public static void assertTrue(boolean value) {
        if (!value) {
            throw new RuntimeException("Expected true");
        }
    }
}