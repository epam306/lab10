public class List {

    private int[] array;

    public List() {
        array = new int[0];
    }

    public void addElementAtIndex(int index, int element) {
        if (index > array.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bound");
        }
        int[] tempArray;
        tempArray = array;
        array = new int[array.length + 1];

        for (int i = 0; i < index; i++) {
            array[i] = tempArray[i];
        }
        array[index] = element;
        for (int i = index + 1; i < array.length; i++) {
            array[i] = tempArray[i - 1];
        }

    }

    public void deleteElementAtIndex(int index) {
        if (index >= array.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bound");
        }
        int[] tempArray;
        tempArray = array;
        array = new int[array.length - 1];
        for (int i = 0; i < index; i++) {
            array[i] = tempArray[i];
        }
        for (int i = index; i < array.length; i++) {
            array[i] = tempArray[i + 1];
        }
    }

    public void replaceElementAtIndex(int index, int element) {
        if (index >= array.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bound");
        }
        array[index] = element;
    }

    public void printArray() {
        for (int element : array) {
            System.out.println(element);
        }
    }

    public int[] getArray() {
        return array;
    }

}
